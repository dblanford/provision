provision
=========

vm guest scripts
----------------
Bash provisioning scripts for Ubuntu VM are stored in `vm_guest`. Includes LAMP installation
plus phpmyadmin, phpunit, xdebug and more. `provisioin.sh` runs the scripts, and can be
used to include or exclude scripts. `config.sh` has all the configuration.

Be sure to read the comments in `provision.sh`

vm host scripts
---------------
Bash scripts for adding to and deleting from the host file of OSX as well as some 
convenient scripts for starting, stopping, etc. parallels vm contained in `vm_host`. Files
are structured the same way with a `config.sh` and `provision.sh`
