#!/usr/bin/env bash

## VM Host Hose file update script
# This script adds an entry for the vm to the hosts file on recient versions of osx.


# search for ip in hosts
hosts_result=$(grep "$vm_client_ip" /private/etc/hosts)

 if [[ -n $hosts_result ]];
 then

    ip_assigned_to=$( echo "$hosts_result" | grep -o '[a-zA-Z0-9-]*\.[a-zA-Z0-9]*$' )

    echo "IP Address already assigned to $ip_assigned_to"

 else

    ## !!! figure out how to fix all occurances. this only gets the first one in a line
    project_name_hyph=$(echo "$project_name" | sed 's/_/-/g' )

    echo "$vm_client_ip  $project_name_hyph.dev" >> /private/etc/hosts

    echo "$vm_client_ip  $project_name_hyph.dev added to hosts file"
 fi


