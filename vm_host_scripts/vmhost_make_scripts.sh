#!/bin/bash


full_script_path="$script_path/.."


# if vm_start does no exist
if [[ ! -f $full_script_path/vm_start ]];
then

    ## Make vm_start
    echo '#!/bin/bash' > $full_script_path/vm_start
    echo "source $script_path/config.sh" >> $full_script_path/vm_start
    echo 'prlctl start $project_name' >> $full_script_path/vm_start
    chmod 755 $full_script_path/vm_start


    ## Make vm_stop
    echo '#!/bin/bash' > $full_script_path/vm_stop
    echo "source $script_path/config.sh" >> $full_script_path/vm_stop
    echo 'prlctl stop $project_name' >> $full_script_path/vm_stop
    chmod 755 $full_script_path/vm_stop

    ## Make vm_ssh
    echo '#!/bin/bash' > $full_script_path/vm_ssh
    echo "source $script_path/config.sh" >> $full_script_path/vm_ssh
    echo 'ssh provisioner@$vm_client_ip' >> $full_script_path/vm_ssh
    chmod 755 $full_script_path/vm_ssh


    ## Make vm_status
    echo '#!/bin/bash' > $full_script_path/vm_status
    echo "source $script_path/config.sh" >> $full_script_path/vm_status
    echo 'prlctl list --all' >> $full_script_path/vm_status
    chmod 755 $full_script_path/vm_status

else
	echo "VM Host scripts already created"
fi

