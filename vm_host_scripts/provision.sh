#!/bin/sh

## Provisioning Shell Script For Host of VM ##

# These scripts are written for OSX 10.9  Host with Parallels 8.x

## IMPORTANT ##
# Before running this script, make sure you run the VM and log in through the console
# AND have all the proper information in the config.sh (vm_temp_ip)
#

# make sure we are running as root
if [ "$UID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

# Path to scripts
# Use this if provision.sh is not in the same directory as other scripts
#script_path='/path/to/scripts/'

if [[ -z $script_path ]];
then
    script_path=$(pwd)
fi

echo "script_path set to $script_path"


# Configuration details
source $script_path/config.sh

# Add VM to host file
source $script_path/vmhost_host_add.sh

# Add VM to host file
source $script_path/vmhost_make_scripts.sh

# Create the VM to host file
# This does not work for some reason
# source $script_path/vmhost_clone_vm.sh
