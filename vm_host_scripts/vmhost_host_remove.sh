#!/usr/bin/env bash

## VM Host Hose file update script
# This script removes the entry for the vm from the hosts file on recient versions of osx.


# search for ip in hosts
hosts_result=$(grep "$vm_client_ip" /private/etc/hosts)


 if [[ -n $hosts_result ]];
 then

    sed -i '.bak' "/$vm_client_ip/d" /private/etc/hosts
    echo "$vm_client_ip entry removed from hosts"
 else

    echo "hosts file does not contain an entry for $vm_client_ip"

 fi

