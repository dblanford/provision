#!/bin/sh
## Shell Script Configuration Details ##

# Name of the VM you want to clone
proto_vm_name='percise-server64'

# Name of the project. Used for: mysql database name, postfix mailname,
project_name='provision' #required

# Static IP address for the VM
# this is the IP that the VM will be set up to use
vm_client_ip='10.211.55.101' #required

