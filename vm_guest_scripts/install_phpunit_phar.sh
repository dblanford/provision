#!/bin/bash

## Install PHPUnit ##

# If PHPUnit does not exist
if [[ ! -f /usr/local/bin/phpunit.phar ]];
then

	# Download PHPUnit
    wget https://phar.phpunit.de/phpunit.phar

    chmod +x phpunit.phar

	# Move it where is it in the path so it can be used globally
    mv phpunit.phar /usr/local/bin/

	ln -s /usr/local/bin/phpunit.phar /usr/local/bin/phpunit

    echo "PHPUnit installed"

else
	echo "PHPUnit already installed"
fi

