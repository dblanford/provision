#!/bin/bash

## Install Composer ##

# If Composer does not exist
if [[ ! -f /usr/local/bin/composer.phar ]];
then

	# Check php settings, download composer
    curl -sS https://getcomposer.org/installer | php

	# Move it where is it in the path so it can be used globally
	mv $script_path/composer.phar /usr/local/bin/
	ln -s /usr/local/bin/composer.phar /usr/local/bin/composer

    echo "Composer installed"

else
	echo "Composer already installed"
fi

