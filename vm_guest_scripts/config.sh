#!/bin/sh
## Shell Script Configuration Details ##

# Name of the project. Used for: mysql database name, postfix mailname,
project_name='provision' #required

# Mysql Root Password
mysql_password='password' #required

# Package repository used by apt (in etc/apt/sources.list)
# comment it out to use default repository
ubuntu_mirror='ubuntu.cs.utah.edu'

# Timezone used to dispaly date/time info. Server clock is set to UTC
# comment it out if you don't want to use it
time_zone='US/Mountain'

# path to sql dump of the database on the guest VM.
# comment it out if you do not want to install a database from sql dump
#db_path='/var/www/db'

# hide index.php: make it so that the index.php is not used in the url
# should be 0 or 1
hide_index=1

# email address displayed on apache server errors
# comment it out if you don't want to use it
admin_email='dave@daveandval.org'

# Static IP address for the VM
# this is the IP that the VM will be set up to use
vm_client_ip='10.211.55.101' #required

# IP address of the VM host in the VM network, used in xdebug config
# comment it out if you are not installing xdebug
vm_host_ip='10.211.55.2' #required for xdebug install

vm_netmask='255.255.255.0'
vm_gateway='10.211.55.1'
vm_nameserver='8.8.8.8 8.8.4.4'

