#!/bin/bash

operating_system=$(lsb_release -si)
os_version=$(lsb_release -sr)


if [[ $operating_system != 'Ubuntu' ]];
then
    echo "Scripts only tested on Ubuntu 12.04 and 14.04"
    exit 1

elif [[ $os_version == '12.04' ]];
then
    echo "os: $operating_system"
    echo "version: $os_version"


elif [[ $os_version == '14.04' ]];
then
    echo "os: $operating_system"
    echo "version: $os_version"

else
    echo "Scripts only tested on Ubuntu 12.04 and 14.04"
    exit 1
fi