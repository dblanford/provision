#!/bin/sh
## setup_apache script ##

# apache2 virtual host config file name
if [[ $os_version == '14.04' ]];
then
    vhost_config_filename='000-default.conf'
else
    vhost_config_filename='default'
fi

# Make sure apache is installed
if [[ -f /etc/apache2/apache2.conf ]];
then

    # First check if perl is installed.
     if [[ -f '/usr/bin/perl' ]];
     then

            # Fix Domain Name
            # This may work for 12.04 but I have not tested it
            if [[ $os_version == '14.04' ]];
            then
                echo "ServerName localhost" >> /etc/apache2/apache2.conf
            fi

            # if a copy of the origional sites config has not been made
            if [[ ! -f "/etc/apache2/sites-available/$vhost_config_filename.origional" ]];
            then
                # Make a copy
                cp /etc/apache2/sites-available/$vhost_config_filename /etc/apache2/sites-available/$vhost_config_filename.origional
            fi


            # Hide Index
            if [[ -n $hide_index ]] && \
                [[ $hide_index -ne 0 ]];
            then

                     #rewrite this so it finds the Allowoverrides for /var/www
                     perl -0777 -pi -e \
                     's/(\s<Directory \/var\/www\/>.*?AllowOverride )None/$1all/s' \
                     /etc/apache2/sites-available/$vhost_config_filename
            fi


            # Change Admin Email
            if [[ -n $admin_email ]]
            then

                     #Change Email
                     perl -0777 -pi -e \
                     "s/ServerAdmin webmaster\@localhost/ServerAdmin $admin_email/" \
                     /etc/apache2/sites-available/$vhost_config_filename
            fi

     else

        echo "Perl is not installed. Cannot modify sites-available/$vhost_config_filename"

     fi

    ## Fix mcrypt config if 14.04
    if [[ $os_version == '14.04' ]];
    then
        ln -s ../../mods-available/mcrypt.ini /etc/php5/apache2/conf.d/20-mcrypt.ini
        php5enmod mcrypt
    fi

    ## Make var/www work properly
  	adduser provisioner www-data
  	chown -R www-data:www-data /var/www
    chmod -R 775 /var/www

    ## enable mod_rewrite
    a2enmod rewrite

	## Restart apache
    /etc/init.d/apache2 restart

else

    echo 'Apache not installed. Cannot modify default site configuration'

fi
