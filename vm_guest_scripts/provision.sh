#!/usr/bin/env bash

## Provisioning Shell Script For Guest VM ##

# These scripts are written for Parallels Desktop based box containing:
#    - Ubuntu 12.04
#    - OpenSSH
#    - "provisioner" user & password

# See http://docs.vagrantup.com/v2/boxes/base.html
# for more info about base box configuration.

## Portions of these scripts come from https://github.com/mauserrifle/vagrant-debian-shell

# make sure we are running as root
if [ "$UID" -ne 0 ]
  then echo "Please run as root"
  exit
fi


# Path to scripts
# Use this if provision.sh is not in the same directory as other scripts
#script_path='/path/to/scripts/'

if [[ -z $script_path ]];
then
    script_path=$(pwd)
fi

echo "script_path set to $script_path"

# Configuration details
source $script_path/config.sh

# check script
source $script_path/check_os.sh

# Before install script
source $script_path/install_before.sh

# Install LAMP stack (includes postfix)
source $script_path/install_lamp.sh

# Install PHPMyAdmin
source $script_path/install_phpmyadmin.sh

# Install Composer
source $script_path/install_composer.sh

# Install PHPUnit
source $script_path/install_phpunit_phar.sh

# Install XDebug
source $script_path/install_xdebug.sh

# After install script
source $script_path/install_after.sh


## Way to make a block comments in a shell script
# needs the single quote to work properly

## START Block Comment ##
: << 'end_of_comment'

--Stuff that gets commented out--

## END Block Comment ##
end_of_comment
