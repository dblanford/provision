#!/bin/sh
## install_after script ##


## Import SQL Database
    source $script_path/vmguest_db_import.sh

## Run setup apache script
    source $script_path/setup_apache.sh

## Run setup apache script
    source $script_path/setup_static_ip.sh

## Run setup apache script
    source $script_path/setup_hostname.sh


## Create phpinfo
    if [[ ! -f '/var/www/phpinfo.php' ]];
    then
     	echo '<?php phpinfo();' > /var/www/phpinfo.php
	    chmod 755 /var/www/phpinfo.php
    fi

## remove apt-get packages
	apt-get -y clean

## Uncomplicated Firewall (UFW)

    # Get ufw status
    ufw_status=$(ufw status | grep Status)

    # if ufw is not active
    if [[ $ufw_status != 'Status: active' ]];
    then
        ufw allow 22
        ufw allow 80
        echo 'y' | sudo ufw enable
    elif  [[ $ufw_status == 'Status: active' ]];
    then
        echo "UFW is active"
    fi

