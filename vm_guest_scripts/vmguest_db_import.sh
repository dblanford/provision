#!/bin/sh
## Import SQL Database

    if [[ ! -n $db_path ]];
    then
        echo "DB path not set in config.sh"

	elif [[ ! -f "$db_path/$project_name.sql" ]];
	then
		echo "$project_name.sql file does not exist"

	elif [[ -d "/var/lib/mysql/$project_name" ]];
	then
		echo "Database $project_name already exists"

	else
   		# Import db into mysql
   		mysql \
		  --user='root' \
		  --password=$mysql_password \
		  < "$db_path/$project_name.sql"

        echo "Database $project_name imported into mysql"

		# For reference
		# Below is the command used create the dump file
		# mysqldump \
		#  --user='root' \
		#  --password=$mysql_password \
		#  --databases $project_name \
		#  > "$db_path/$project_name.sql"

    fi