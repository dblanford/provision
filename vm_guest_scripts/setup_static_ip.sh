#!/bin/sh

## Setup VM Guest with Static IP ##

# Check if this scrip has already run
if [[ ! -f /etc/network/interfaces.original ]]
then

    # shut down eth0, which does dhcp release
    ifdown eth0

    # rename the origional file
    mv /etc/network/interfaces /etc/network/interfaces.original

    # make the new file
    echo "auto lo" > /etc/network/interfaces
    echo "iface lo inet loopback" >> /etc/network/interfaces
    echo " " >> /etc/network/interfaces
    echo "auto eth0" >> /etc/network/interfaces
    echo "iface eth0 inet static" >> /etc/network/interfaces
    echo "    address $vm_client_ip" >> /etc/network/interfaces
    echo "    netmask $vm_netmask" >> /etc/network/interfaces
    echo "    gateway $vm_gateway" >> /etc/network/interfaces
    echo "    dns-nameservers $vm_nameserver" >> /etc/network/interfaces
    echo " " >> /etc/network/interfaces

    # bring up eth0 with the new static config
    ifup eth0

    if [ "$?" -ne 0 ]
    then
        echo "Something went wrong, the ifup exit code is = $?"
        exit
    fi

    echo "IP set to $vm_client_ip"
else
    echo "Static IP already set"
fi
