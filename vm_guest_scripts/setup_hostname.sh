#!/bin/bash


# First check if perl is installed.
 if [[ -f '/usr/bin/perl' ]];
 then

        # if a copy of the origional files
        if [[ ! -f '/etc/hosts.origional' ]];
        then
            # Make a copy
            cp /etc/hosts /etc/hosts.origional

             #change the domain name in /etc/hosts
             perl -0777 -pi -e \
             "s/127\.0\.1\.1.*/127.0.1.1\t$project_name/" \
             /etc/hosts

            # Write the new hostname to file
            # so it sticks after reboot
            echo "$project_name" > /etc/hostname
            echo " " >> /etc/hostname

            # this sets it to new hostname before the reboot
            hostname $project_name

            echo "hostname set to $project_name"

        else

            echo "hostname already set to $project_name"
        fi

 else

    echo 'Perl is not installed. Cannot modify etc/hosts'

 fi
