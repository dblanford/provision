#!/bin/sh

## SQL Database Export

# make sure we are running as root
if [ "$UID" -ne 0 ]
  then echo "Please run as root"
  exit
fi


# Path to scripts
# Use this if vmguest_db_export.sh is not in the same directory as other scripts
#script_path='/path/to/scripts/'

if [[ -z $script_path ]];
then
    script_path=$(pwd)
fi

echo "script_path set to $script_path"


# Configuration details
source $script_path/config.sh

# get the date
date=$(date +"%Y-%m-%d_%k:%m:%S")


# if database exists and $db_path is set
if [[ ! -d "/var/lib/mysql/$project_name" ]];
then

    echo "$project_name database does not exist"

elif [[ ! -n $db_path ]];
then

    echo "db_path variable inot set in $project_name/config.sh"

else

    # For Reference
    # Import db into mysql
    # mysql \
    #   --user='root' \
    #   --password=$mysql_password \
    #   < "$db_path/$project_name.sql"

    # create a sql database dump file

    # dump the db with the date
    # the files with the dates in their names never get deleted
    # so you always have a set of all the dbs that you have dumped
    # the current dump also gets copied to a name without a date
    # which is convienent if you are using version control
    mysqldump \
      --user='root' \
      --password=$mysql_password \
      --databases $project_name \
      > "$db_path/$project_name$date.sql"

    # if there is already a curren file del
    if [[ -f "$db_path/$project_name.sql" ]];
    then
        rm "$db_path/$project_name.sql"
    fi

    cp "$db_path/$project_name$date.sql" "$db_path/$project_name.sql"

fi

